**HW2-a**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 1V
Vin Vin Gnd pulse 0V 1V 0.3ns 0.4ns 0.4ns 0.6ns 2ns

.subckt inv In Out
MP Out In Vdd Vdd pMOS w=1u l=0.18u
MN Out In Gnd Gnd nMOS w=1u l=0.18u
.ends

xn1 Vin net1 inv
xn2 net1 net2 inv
xn3 net2 net3 inv
xn4 net3 net4 inv
xn5 net4 net5 inv
xn6 net5 net6 inv
xn7 net6 net7 inv
xn8 net7 net8 inv
xn9 net8 net9 inv
xn10 net9 net10 inv
xn11 net10 net11 inv
xn12 net11 net12 inv
xn13 net12 net13 inv
xn14 net13 net14 inv
xn15 net14 net15 inv
xn16 net15 net16 inv
xn17 net16 net17 inv
xn18 net17 net18 inv
xn19 net18 net19 inv
xn20 net19 net20 inv
xn21 net20 net21 inv
xn22 net21 net22 inv
xn23 net22 net23 inv
xn24 net23 net24 inv
xn25 net24 net25 inv
xn26 net25 net26 inv
xn27 net26 net27 inv
xn28 net27 net28 inv
xn29 net28 net29 inv
xn30 net29 net30 inv
xn31 net30 net31 inv
xn32 net31 net32 inv
xn33 net32 net33 inv
xn34 net33 net34 inv
xn35 net34 net35 inv
xn36 net35 net36 inv
xn37 net36 net37 inv
xn38 net37 net38 inv
xn39 net38 net39 inv
xn40 net39 net40 inv
xn41 net40 net41 inv
xn42 net41 net42 inv
xn43 net42 net43 inv
xn44 net43 net44 inv
xn45 net44 net45 inv
xn46 net45 net46 inv
xn47 net46 net47 inv
xn48 net47 net48 inv
xn49 net48 net49 inv
xn50 net49 net50 inv
xn51 net50 net51 inv
xn52 net51 net52 inv
xn53 net52 net53 inv
xn54 net53 net54 inv
xn55 net54 net55 inv
xn56 net55 net56 inv
xn57 net56 net57 inv
xn58 net57 net58 inv
xn59 net58 net59 inv
xn60 net59 net60 inv
xn61 net60 net61 inv
xn62 net61 net62 inv
xn63 net62 net63 inv
xn64 net63 Vout inv

.tran 1ns 10ns
.option post=1
.temp 25

.end