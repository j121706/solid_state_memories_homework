**NAND gate**
.inc "D:\synopsys\65nm_bulk.pm"
MP1 vout   vain vdd    vdd   pmos  W=3u  L=0.065u
MP2 vout   vbin vdd    vdd   pmos  W=3u  L=0.065u
MN1 vout   vain NET1   NET1  nmos  W=1u  L=0.065u
MN2 NET1   vbin  gnd   gnd   nmos  W=1u  L=0.065u

vdd vdd gnd dc 1
vain  vain gnd pulse(0  1 0 0.5n 0.5n 1n 2n)
vbin  vbin gnd pulse(0  1 0 0.5n 0.5n 2n 4n)

.tran 0.1ns 6ns
.option post=1

.print v(vout)
.print v(vain)
.print v(vbin)
.end
