**HW1-b_0.8V**
.inc "D:\synopsys\65nm_bulk.pm"
Vdd Vdd Gnd dc 0.8V
Vin Vin Gnd dc 0.8V

MP Vout Vin Vdd Vdd pMOS w=1u l=0.18u
MN Vout Vin Gnd Gnd nMOS w=1u l=0.18u

.dc Vin 0V 0.8V 0.05V 
.option post=1
.temp 25

.print v(Vout)
.end

