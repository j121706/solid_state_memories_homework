**HW1-b_1V**
.inc "D:\synopsys\65nm_bulk.pm"
Vdd Vdd Gnd dc 1V
Vin Vin Gnd dc 1V

MP Vout Vin Vdd Vdd pMOS w=1u l=0.18u
MN Vout Vin Gnd Gnd nMOS w=1u l=0.18u

.dc Vin 0V 1V 0.05V 
.option post=1
.temp 25

.print v(Vout)
.end

