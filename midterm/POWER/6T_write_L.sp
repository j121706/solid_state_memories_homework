**mid_6t_AC_write_1to0**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 0.8v

VBL BL Gnd PWL  0n 0v,8n 0v
** 2ns pulse
VBLB BLB Gnd PWL 4n 0v,4.01n 0.8v,7n 0.8v,7.01n 0v
VWL WL Gnd PWL  4n 0v,4.01n 0.8v,8n 0.8v,8.01n 0v


**MP nd ng ns <nb> name w l
M1 Qb Q Vdd Vdd pMOS w=1u l=0.18u
M2 Qb Q Gnd Gnd nMOS w=2u l=0.18u
M3 Q Qb Vdd Vdd pMOS w=1u l=0.18u
M4 Q Qb Gnd Gnd nMOS w=2u l=0.18u

M5 BLB WL Qb Qb nMOS w=1u l=0.18u
M6 BL WL Q Q nMOS w=1u l=0.18u

** initail condition
.ic V(Qb)=0v
.ic V(Q)=0.8v

.plot I1(M5)
.plot I1(M6)

.tran 0.1ns 20ns
.option post=1
.temp 25

.end