**mid_8t_AC_read**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 0.8v
** 2ns pulse
Vpulse RWL Gnd PWL 1n 0v,1.01n 0.8v,3n 0.8v,3.01n 0v

**MP nd ng ns <nb> name w l
M1 Qb Q Vdd Vdd pMOS w=1u l=0.18u
M2 Qb Q Gnd Gnd nMOS w=2u l=0.18u
M3 Q Qb Vdd Vdd pMOS w=1u l=0.18u
M4 Q Qb Gnd Gnd nMOS w=2u l=0.18u

M5 BLB WL Qb Qb nMOS w=1u l=0.18u
M6 BL WL Q Q nMOS w=1u l=0.18u

M7 RBL RWL Con Con nMOS w=2u l=0.5u
M8 Con Qb Gnd Gnd nMOS w=2u l=0.18u

** initail condition -- 1, Qb = 0
.ic V(Qb)=0v
.ic V(Q)=0.8v
** initail condition -- 1, Qb = 1
*.ic V(Q)=0v
*.ic V(Qb)=0.8v
.ic V(RBL)=0.8v

.plot I1(M5)
.plot I1(M6)
.plot I1(M8)

.tran 0.05ns 5ns
.option post=1
.temp 25

.end