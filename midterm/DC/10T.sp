**mid_10t_DC_read**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 1v
VGnd Gnd 0 0

** initail condition
VBLB BLB Gnd dc 1v
VBL BL Gnd dc 0v
VWL WL Gnd dc 1v
VQ Q Gnd DC 1v
VQB1 QB1 Gnd DC 0V

** MP nd ng ns <nb> name w l
** PART 1
M1 Qb Q Vdd Vdd pMOS w=1u l=0.065u
M2 Qb Q nodeM27 nodeM27 nMOS w=0.8u l=0.065u
M5 BLB WL Qb Gnd nMOS w=1u l=0.065u

** PART 2
M3 Q1 Qb1 Vdd Vdd pMOS w=1u l=0.065u
M4 Q1 Qb1 nodeM48 nodeM48 nMOS w=0.8u l=0.065u
M6 BL WL Q1 Gnd nMOS w=1u l=0.065u

M7 nodeM27 Q Gnd Gnd nMOS w=1u l=0.18u
M8 nodeM48 Qb1 Gnd Gnd nMOS w=1u l=0.18u

M9 Vdd Qb nodeM27 nodeM27 nMOS w=2u l=0.18u
M10 Vdd Q1 nodeM48 nodeM48 nMOS w=2u l=0.18u

.dc VQ 0V 1v 0.01V
.print dc v(QB) 

.dc VQB1 0V 1v 0.01V
.print dc v(Q1) 

.option post=1
.option INGOLD=2
.temp 25

.end