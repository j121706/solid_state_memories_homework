**mid_8t_DC_read**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 0.4v
VGnd Gnd 0 0

** initail condition
VBL BL Gnd dc 0v
VBLB BLB Gnd dc 0v
VWL WL Gnd dc 0v
VRBL RBL Gnd dc 0.4v
VRWL RWL Gnd dc 0.4v

VQ Q Gnd DC 0.4v
VQB1 QB1 Gnd DC 0V

** MP nd ng ns <nb> name w l
** PART 1
M1 Qb Q Vdd Vdd pMOS w=1u l=0.065u
M2 Qb Q Gnd Gnd nMOS w=0.8u l=0.065u
M5 BLB WL Qb Gnd nMOS w=1u l=0.065u

** PART 2
M3 Q1 Qb1 Vdd Vdd pMOS w=1u l=0.065u
M4 Q1 Qb1 Gnd Gnd nMOS w=0.8u l=0.065u
M6 BL WL Q1 Gnd nMOS w=1u l=0.065u

M7 RBL RWL Con Gnd nMOS w=1.5u l=0.065u
M8 Con Qb Gnd Gnd nMOS w=1.5u l=0.18u

.dc VQ 0V 0.4v 0.01V
.print dc v(QB) 

**.dc VQB1 0V 0.4v 0.01V
**.print dc v(Q1) 

.option post=1
.option INGOLD=2
.temp 25

.end