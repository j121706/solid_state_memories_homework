**mid_6t_AC_read**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 0.8v

** 2ns pulse
Vpulse WL Gnd PWL 1n 0v,1.01n 0.8v,3n 0.8v,3.01n 0v

** MP nd ng ns <nb> name w l
M1 Qb Q Vdd Vdd pMOS w=1u l=0.18u
M2 Gnd Q Qb Gnd nMOS w=1.5u l=0.18u
M3 Q Qb Vdd Vdd pMOS w=1u l=0.18u
M4 Gnd Qb Q Gnd nMOS w=1.5u l=0.18u

M5 BLB WL Qb Qb nMOS w=1u l=0.18u
M6 BL WL Q Q nMOS w=1u l=0.18u

** initail condition
.ic V(Qb)=0v
.ic V(Q)=0.8v
.ic V(BL)=0.8v
.ic V(BLB)=0.8sV

.tran 0.05ns 4ns
.option post=1
.temp 25

.end