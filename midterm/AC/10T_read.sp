**mid_10t_AC_read**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 0.8v
** 2ns pulse
VWL WL Gnd PWL  1n 0v,1.01n 0.8v,3n 0.8v,3.01n 0v

**MP nd ng ns <nb> name w l
M1 Qb Q Vdd Vdd pMOS w=1u l=0.18u
M2 Qb Q nodeM27 nodeM27 nMOS w=2u l=0.18u
M7 nodeM27 Q Gnd Gnd nMOS w=2u l=0.18u

M3 Q Qb Vdd Vdd pMOS w=1u l=0.18u
M4 Q Qb nodeM48 nodeM48 nMOS w=2u l=0.18u
M8 nodeM48 Qb Gnd Gnd nMOS w=2u l=0.18u

M5 BLB WL Qb Qb nMOS w=1u l=0.18u
M6 BL WL Q Q nMOS w=1u l=0.18u

M9 Vdd Qb nodeM27 nodeM27 nMOS w=2u l=0.18u
M10 Vdd Q nodeM48 nodeM48 nMOS w=2u l=0.18u

.ic V(Qb)=0v
.ic V(Q)=0.8v
.ic V(BL)=0.8v
.ic V(BLB)=0.8V

.tran 0.1ns 4ns
.option post=1
.temp 25

.end